*** Settings ***
Test Setup        Log To Console    <<<START>>>
Test Teardown     Log To Console    ------------------------------------------------------------------------------
Resource          ../KEYWORDS/Keywords.txt

*** Test Cases ***
WS2_ROBOT
    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    DATA1    2
    [W] Open Browser    https://www.advantageonlineshopping.com/#/
    [W] Capture Page Screenshot
    [W] Click Element    //a[@id='hrefUserIcon']//*[local-name()='svg']
    [W] Capture Page Screenshot
    Sleep    3
    [W] Input Text    //input[@name='username']    kachaporn
    [W] Capture Page Screenshot
    Sleep    3
    [W] Input Text    //input[@name='password']    Passw0rd
    [W] Capture Page Screenshot
    Sleep    3
    [W] Click Element    //sec-sender[@class='roboto-medium ng-isolate-scope sec-sender']
    [W] Capture Page Screenshot
    [W] Click Element    //div[@id='miceImg']
    [W] Capture Page Screenshot
    [W] Click Element    //img[@id='32']
    [W] Capture Page Screenshot
    [W] Click Element    //div[@id='Description']//div[2]//span[2]
    [W] Capture Page Screenshot
    [W] Input Text    //input[@name='quantity']    10
    [W] Capture Page Screenshot
    [W] Click Element    //button[@name='save_to_cart']
    [W] Capture Page Screenshot
    [W] Click Element    //a[@id='shoppingCartLink']//*[local-name()='svg']
    [W] Capture Page Screenshot
    [W] Click Element    //button[@id='checkOutButton']
    [W] Capture Page Screenshot
    Sleep    3
    [W] Check Meaasge    ORDER PAYMENT
    [W] Capture Page Screenshot
    [W] Click Element    //button[@id='next_btn']
    [W] Capture Page Screenshot
    [W] Click Element    //button[@id='pay_now_btn_MasterCredit']
    [W] Capture Page Screenshot
    [W] Check Meaasge    Thank you for buying with Advantage
    [W] Capture Page Screenshot
    [W] Click Element    //span[@class='hi-user containMiniTitle ng-binding']
    [W] Capture Page Screenshot
    [W] Click Element    //label[@class='option roboto-medium ng-scope'][contains(text(),'Sign out')]
    [W] Capture Page Screenshot
    [W] Close Browser

TC02_TESTAPI
    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    DATA3    2
    Log    @{ListData}[0]
    Log    @{ListData}[1]
    Log    @{ListData}[2]
    [AI]Get Request Data    @{ListData}[0]    @{ListData}[1]    @{ListData}[2]

TC03_TESTPOST
    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    DATA4    2
    Log    @{ListData}[0]
    Log    @{ListData}[1]
    [AI]Post Request Data    @{ListData}[0]    @{ListData}[1]

TC00_GetJobJenkins
    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    Jenkins    2
    @{Auth}=    Create List    @{ListData}[0]    @{ListData}[1]
    [AI]Get Job Jenkins    @{ListData}[2]    @{ListData}[3]    @{ListData}[4]    @{Auth}

TC001_DogRandomFernGet
    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    DATA4    2
    Log    @{ListData}[0]
    Log    @{ListData}[1]
    [AI]Get Request Data    @{ListData}[0]    @{ListData}[1]

TC002_EmployeeFernPost
    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    DATA3    2
    Log    @{ListData}[0]
    Log    @{ListData}[1]
    Log    @{ListData}[2]
    [AI]Post Request Data    @{ListData}[0]    @{ListData}[1]    @{ListData}[2]
