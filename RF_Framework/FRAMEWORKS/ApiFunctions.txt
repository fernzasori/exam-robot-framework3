*** Settings ***
Resource          Library.txt

*** Keywords ***
[AI]Get Request Data
    [Arguments]    ${URL}    ${AppName}
    Set Library Search Order    RequestsLibrary
    Create Session    URL    ${URL}
    ${response}    Get Request    URL    ${AppName}
    Log To Console    response ==> ${response}
    Log To Console    sesp -->${response.json()["status"]}
    Should Be Equal As Strings    ${response.json()["status"]}    success
    Should Be Equal As Strings    ${response.status_code}    200
    Comment    Log To Console    sesp -->${response.json()["message"]}
    Comment    Should Be Equal As Strings    ${response.json()["message"]}    https://images.dog.ceo/breeds/spaniel-brittany/n02101388_9127.jpg

[AI]Post Request Data
    [Arguments]    ${URL}    ${AppName}    ${JsonCode}
    Set Library Search Order    RequestsLibrary
    ${headers}=    Create Dictionary    content-type=application/json    charset=UTF-8    User-Agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36
    Create Session    URL    ${URL}
    ${resp}    Post Request    URL    ${AppName}    ${JsonCode}    headers=${headers}
    Log To Console    resp ==> ${resp}
    Should Be Equal As Strings    ${resp.status_code}    200
    Log To Console    sesp -->${resp.json()["status"]}
    Should Be Equal As Strings    ${resp.json()["status"]}    success
    Log To Console    Response Json -->${resp.json()}
    Return From Keyword    ${resp}

[AI]Get Job Jenkins
    [Arguments]    ${URL}    ${AppName}    ${Param}    @{Auth}
    Set Library Search Order    RequestsLibrary
    Create Session    URL    ${URL}    auth=@{Auth}
    ${response}    Get Request    URL    ${AppName}    params=${Param}
    Log To Console    response ==> ${response}
    Should Be Equal As Strings    ${response.status_code}    200
    Log To Console    fullDisplayName-->${response.json()["fullDisplayName"]}
    Should Be Equal As Strings    ${response.json()["fullDisplayName"]}    KACHAPORN » EXAM_JERN
    Log To Console    url -->${response.json()["primaryView"]["url"]}
    Should Be Equal As Strings    ${response.json()["primaryView"]["url"]}    http://64.227.108.197:8080/job/KACHAPORN/job/EXAM_JERN/
